var datamodels= require('../dataModels/datamodels.js');
var mongoose= require('mongoose');
var mongoURL= 'mongodb://localhost/patientcare'
var dbConnection= mongoose.createConnection(mongoURL,{server: { poolSize: 100 }});

var vitalsModel= dbConnection.model('vitals', datamodels.getVitalsSchema(mongoose.Schema));
var consumerModel= dbConnection.model('consumer', datamodels.getConsumerSchema(mongoose.Schema));
var activityModel= dbConnection.model('activities', datamodels.getActivitySchema(mongoose.Schema));
var carePlanModel= dbConnection.model('careplan', datamodels.getCareplanSchema(mongoose.Schema));
var developerModel=dbConnection.model('developers', datamodels.getDeveloperSchema(mongoose.Schema));

exports.getConsumerModel= function(mongoose){
	return consumerModel;
}

exports.getCarePlanModel= function(){
	return carePlanModel;
}

exports.getVitalsModel= function(mongoose){
	return vitalsModel;
}


exports.getActivityModel= function(mongoose){
  return activityModel;
}

exports.getCareplanModel= function(mongoose){
  return careplanModel;
}

exports.getDeveloperModel= function(){
    return developerModel;
}

exports.saveDB= function(modelToSave, callback){
    modelToSave.save(function (err, updateStatus){
        callback(err, updateStatus);
    });
}

exports.updateDB= function(modelToUpdate, updateCondition,updateValue, isUpsert, callback){
    modelToUpdate.update(updateCondition, updateValue, {upsert:isUpsert}, function (err, updateStatus){
        callback(err, updateStatus);
    });
}

// exports.updateDB= function(modelToUpdate, updateCondition,updateValue, isUpsert, callback){
//   modelToUpdate.update(updateCondition, updateValue, {upsert:isUpsert}, function (err, updateStatus){
//     callback(err, updateStatus);
//   });
// }

exports.deleteFromDB= function(modelToUpdate, IDToDelete, callback){
  modelToUpdate.findByIdAndRemove(IDToDelete, function (err, updateStatus){
    callback(err, updateStatus);
  });
}
