var mongooseHelper= require('./mongooseHelper.js');
var responseHelper= require('./responseHelper');
var devIDChecker= require('./devIDChecker');
var dataModels= require('../dataModels/datamodels.js');
/*
 * Add a new consumer.
 */
exports.add= function(req, res){
  //res.render('index', { title: 'Express Home Page Reloads' });
  var addConsumer= function(checkStatus){
      if (req.body.demographics){
        console.log(req.body.demographics);
        var demographicsInstance= {
          ethnicity:req.body.demographics.ethnicity, 
          age: req.body.demographics.age, 
          gender: req.body.demographics.gender, 
          employment: req.body.demographics.employment, 
          industry: req.body.demographics.industry, 
          when: Date.now
        };     
      }
      if (req.body.personalinfo){
        console.log(req.body.personalinfo);
        var personalInfoInstance= {
          first_name:req.body.personalinfo.first_name, 
          last_name:req.body.personalinfo.last_name, 
          city:req.body.personalinfo.city, 
          state:req.body.personalinfo.state, 
          date_of_birth:req.body.personalinfo.date_of_birth, 
          username:req.body.personalinfo.username, 
          password:req.body.personalinfo.password
        };     
      }
      var consumer= new mongooseHelper.getConsumerModel()({
        "demographics":demographicsInstance,
        "personal_info":personalInfoInstance
      });
      mongooseHelper.saveDB(consumer,function(err, response){
       if (err){
          console.log(err);
          res.send(responseHelper.errorMSG('Error saving consumer')); 
        }
        else
          if (response){
            var consumerID= response._id;
            var developer= new mongooseHelper.getDeveloperModel();
            mongooseHelper.updateDB(developer,{developerID: req.get('DeveloperID')},{$push:{"consumerID": consumerID}},{upsert:true}, 
                                function(err, response){
                                  if (err)
                                    res.send(responseHelper.errorMSG('Error updating consumer' + err)); 
                                  else
                                    if (response)
                                      res.send(responseHelper.successMSG(consumerID));
                                    else
                                      res.send(responseHelper.errorMSG('Error updating consumer. Please check ' + 
                                               'the object structure you have posted.')); 
                                });


          }
          else
            res.send(responseHelper.errorMSG('Could not save consumer. Please check' + 
                                                       ' the schema of the object'));
      });
  }
  devIDChecker.checkDevID(req, addConsumer); 
};

exports.get= function(req, res){
   var queryObj=1;
  console.log( Object.keys(req.query).length);
  if (Object.keys(req.query).length){
     queryObj= req.query['values'];
  }

    if (queryObj)
      try{
        queryObj= JSON.parse(queryObj);
      }
      catch (e){
        res.send('Error parsing inputs');
      }
    else
      queryObj={}
    var consumer= new mongooseHelper.getConsumerModel();
    consumer.find(queryObj, function (err, result){
      if(err){
        console.log('Error');
        console.log(err);
      }else{
       console.log('Got it!');
       res.send(result);
      }
    });
};


